from models.Usuarios import Usuarios, db

class Historial_Puntaje(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    correo = db.Column(db.String(256), db.ForeignKey('usuarios.correo'), nullable=False)
    puntaje = db.Column(db.Integer)
    fecha = db.Column(db.String(16))

    usuarios = db.relationship('Usuarios')

    def __str__(self):
        return (
            f'id: {self.id}, '
            f'Correo: {self.correo}, '
            f'puntaje: {self.puntaje}, '
            f'fecha: {self.fecha}, '
        )