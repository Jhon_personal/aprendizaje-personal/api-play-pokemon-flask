from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Usuarios(db.Model):
    correo = db.Column(db.String(256), primary_key=True)
    usuario = db.Column(db.String(16), unique=True)
    password = db.Column(db.String(256))
    token_access = db.Column(db.Text)
    points = db.Column(db.Integer, default=0, nullable=False)

    def __str__(self):
        return (
            f'Correo: {self.correo}, '
            f'Usuario: {self.usuario}, '
            f'pass: {self.password}, '
            f'token_access: {self.token_access},'
            f'puntos: {self.points}'
        )