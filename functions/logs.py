class Logs():

    def __init__(self, code, msg, data={}):
        self._code = code
        self._msg = msg
        self._data = data

    def log_message(self):
        data_response = {
            'message': self._msg,
            'code': self._code,
            'dataUser': self._data
        }

        return data_response
