import hashlib
from functions.logs import Logs
from models.Usuarios import db
from flask import jsonify


class UsuarioAction():

    def Register(self, data_user):

        try:
            db.session.add(data_user)
            db.session.commit()
            logs = Logs('200', 'Usuario registrado',)
        except Exception as e:
            logs = Logs('404', 'Error registro')

        data_response = logs.log_message()

        return data_response

    @staticmethod
    def codifyPass(password):
        sha256 = hashlib.sha256()
        sha256.update(password.encode('utf-8'))
        hashed_valor = sha256.hexdigest()

        return hashed_valor

    @staticmethod
    def Login(mail, user, password, token):
        data = {
            "correo": mail,
            "usuario": user,
            "pass": password,
            "token": token
        }

        logs = Logs('200', 'Usuario autenticado', data)
        data_response = logs.log_message()

        return data_response


