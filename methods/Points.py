from models.Usuarios import db, Usuarios
from functions.logs import Logs

class PointsAction():

    @staticmethod
    def Register(points_data):

        try:
            db.session.add(points_data)
            db.session.commit()
            logs = Logs('200', 'Puntos registrados')
        except Exception as e:
            logs = Logs('404', 'Error puntos')

        data_response = logs.log_message()

        return data_response

    @staticmethod
    def totalPoints(correo, puntaje):

        try:
            points_user = Usuarios.query.filter_by(correo=correo).first_or_404()

            if puntaje > points_user.points:
                points_user.points = puntaje
                db.session.commit()

            logs = Logs('200', points_user.points)
        except Exception as e:
            logs = Logs('404', 'Error obteniendo puntos')

        data_response = logs.log_message()

        return data_response

    @staticmethod
    def listRanking(usuarios, user = ''):

        data_response = {}

        for index, usuario in enumerate(usuarios, start=1):
            if usuario.usuario == user and user != '':
                data_response[index] = {
                    'usuario': usuario.usuario,
                    'puntos': usuario.points,
                    'correo': usuario.correo
                }
                break
            elif user == '':
                data_response[index] = {
                    'usuario': usuario.usuario,
                    'puntos': usuario.points
                }

        logs = Logs('200', 'List Users', data_response)
        response = logs.log_message()

        return response