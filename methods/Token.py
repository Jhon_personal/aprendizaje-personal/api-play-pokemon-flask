from flask import jsonify
from flask_jwt_extended import create_access_token


class Token():

    @staticmethod
    def get_token(data_user):
        token = create_access_token(identity=data_user, expires_delta=False)
        return jsonify(token)
