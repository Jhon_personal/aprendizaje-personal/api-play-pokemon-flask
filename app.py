import string
import random

from flask import Flask, request
from flask_cors import CORS
from flask_migrate import Migrate
from sqlalchemy import desc

from config import FULL_URL_DB, SECRET_KEY
from flask_jwt_extended import JWTManager, jwt_required
from functions.logs import Logs
from methods.Usuarios import UsuarioAction
from methods.Token import Token
from models.Usuarios import db, Usuarios
from models.Historial_Puntaje import Historial_Puntaje
from methods.Points import PointsAction

app = Flask(__name__)

CORS(app)

app.config['SQLALCHEMY_DATABASE_URI'] = FULL_URL_DB
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)

migrate = Migrate()
migrate.init_app(app, db)

app.config['JWT_SECRET_KEY'] = SECRET_KEY
jwt = JWTManager(app)

usuarioAction = UsuarioAction()

##########################################################################################
# metodos
# Token
@app.route('/token', methods=['POST'])
def token():
    data = request.get_json()
    correo = data.get('CORREO')
    usuario = data.get('USUARIO')

    data_user = {
        "correo": correo,
        "usuario": usuario
    }

    config_values = Token()
    token_value = config_values.get_token(data_user)

    return token_value


@app.route('/tokenInfo/<token>')
@jwt_required()
def tokenInfo(token):

    try:
        data_user = Usuarios.query.filter_by(token_access=token).first_or_404()
        result = usuarioAction.Login(data_user.correo, data_user.usuario, data_user.password, data_user.token_access)
    except Exception as e:
        logs = Logs('404', 'Token Failed')
        result = logs.log_message()

    return result



##########################################################################################
# usuarios
@app.route('/register', methods=['POST'])
@jwt_required()
def Register():
    #usuarioAction = UsuarioAction()
    data = request.get_json()
    authorization_header = request.headers.get('Authorization')
    _, token = authorization_header.split(None, 1)
    correo = data.get('CORREO')
    usuario = data.get('USUARIO')
    password = data.get('PASSWORD')
    password = usuarioAction.codifyPass(password)

    user_data = Usuarios(correo=correo, usuario=usuario, password=password, token_access=token)
    result = usuarioAction.Register(user_data)
    code = result['code']

    if code == '200':
        data_user = Usuarios.query.get_or_404(correo)
        result = usuarioAction.Login(data_user.correo, data_user.usuario, data_user.password, data_user.token_access)

    return result


@app.route('/login', methods=['POST'])
@jwt_required()
def Login():
    #usuarioAction = UsuarioAction()
    data = request.get_json()
    authorization_header = request.headers.get('Authorization')
    _, token = authorization_header.split(None, 1)

    try:
        usuario = data.get('USUARIO')
        password = data.get('PASSWORD')
        password = usuarioAction.codifyPass(password)

        data_user = Usuarios.query.filter_by(usuario=usuario, password=password).first_or_404()
        data_user.token_access = token
        db.session.commit()

        result = usuarioAction.Login(data_user.correo, data_user.usuario, data_user.password, data_user.token_access)
    except Exception as e:
        logs = Logs('404', 'Login Failed')
        result = logs.log_message()

    return result


@app.route('/recovery', methods=['POST'])
@jwt_required()
def Recovery():
    data = request.get_json()
    correo = data.get('CORREO')

    try:
        data_user = Usuarios.query.filter_by(correo=correo).first_or_404()

        characters = string.ascii_letters + string.digits
        pass_string = ''.join(random.choice(characters) for _ in range(8))
        password = usuarioAction.codifyPass(pass_string)

        data_user.password = password
        db.session.commit()

        data = {
            "pass": pass_string
        }
        logs = Logs('200', 'New Password', data)
    except Exception as e:
        logs = Logs('404', 'Recovery Failed')

    result = logs.log_message()

    return result

##########################################################################################
#Records
@app.route('/points', methods=['POST'])
@jwt_required()
def Points():
    data = request.get_json()

    try:
        correo = data.get('CORREO')
        points = data.get('PUNTAJE')
        fecha = data.get('FECHA')

        points_data = Historial_Puntaje(correo=correo, puntaje=points, fecha=fecha )
        result = PointsAction.Register(points_data)
        code = result['code']

        if code == '200':
            result = PointsAction.totalPoints(correo, points)
    except Exception as e:
        logs = Logs('404', 'Register Points Failed')
        result = logs.log_message()

    return result

@app.route('/list')
@jwt_required()
def List():

    usuarios = Usuarios.query.order_by(desc(Usuarios.points)).limit(5).all()
    result = PointsAction.listRanking(usuarios)

    return result

@app.route('/pointsUser/<user>')
@jwt_required()
def pointsUser(user):
    usuarios = Usuarios.query.order_by(desc(Usuarios.points)).all()
    result = PointsAction.listRanking(usuarios, user)

    return result